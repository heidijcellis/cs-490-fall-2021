## CS-490 Software Engineering 
### Instructor: Dr. Heidi Ellis
### Topics and Assignments Will Change
### Updated: 2021-11-22
### Schedule for CS 490 Software Engineering

Date | Reading/Activity Before Class | Class | Due
:-- | :-- | :-- | :--
||**Week 1 - Orientation**|
8/31 <br /> **H103** |- [What is Software Engineering? - Georgia Tech](https://www.youtube.com/watch?v=gBd-ct58DCI) <br /> - Complete "Entrance Survey" on Kodiak | - Organization <br /> - [Opening Thoughts Slides](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Slides/Opening_Thoughts.pdf) <br /> - **Start HW 1 and Team Assn 1**| 
9/2 <br /> **H103**| - Fred Brooks "No Silver Bullet" [in PDF](http://worrydream.com/refs/Brooks-NoSilverBullet.pdf) or [in HTML](http://www.cs.nott.ac.uk/%7Epszcah/G51ISS/Documents/NoSilverBullet.html) <br /> - [Tuckman's stages of group development](https://en.wikipedia.org/wiki/Tuckman%27s_stages_of_group_development)| - [Team Formation Slides](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Slides/Team_Formation.pdf) <br /> - Team Organization  <br /> - [Activity 01 - Triangles and Teams](https://drive.google.com/drive/folders/1CQz8V7w5eheq_kP3FyIL6XCxZJ6stDoL?usp=sharing) <br /> - [Meeting Note Format](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Resources/Meeting_Note_Format.xlsx) <br /> - **Start HW 2 and Team Assn 2** | - [Homework 01 - Onboarding](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Homeworks/Homework_01.odt) Individual submission to Kodiak
||**Week 2 - Software Process**|
9/7 <br /> **H 103**| - [What is the SDLC? (text)](https://blog.testlodge.com/software-development-life-cycle/)<br /> - [What is the SDLC? (video)](https://www.youtube.com/watch?v=cgWzYMaDncI)<br /> - [Steps of the SDLC (video)](https://www.youtube.com/watch?v=gNmrGZSGK1k)|- [Activity 02 - Software Dev Activtites](https://drive.google.com/drive/folders/1ovnHVw9uAuHSWuzOHJtmKP7g0YW6e5Av?usp=sharing) | - [Team Assn 1 - Launch Report](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Project/Assn_1_-_Launch_Report.odt) Team submission to Kodiak <br /> - [Team Meeting Note Format](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Resources/Meeting_Note_Format.xlsx)
9/9 <br /> **Discord**| - [A Rational Design Process (text)](https://users.ece.utexas.edu/%7Eperry/education/SE-Intro/fakeit.pdf) <br />- [Different Project LifeCycles (video)](https://www.youtube.com/watch?v=JFi5dfsTR10) <br />- [Waterfall model (video)](https://www.youtube.com/watch?v=5A5XCuWMG4o) <br />- [Iterative and Incremental model (video)](https://www.youtube.com/watch?v=gcpOZi6Hz38&t=9s) <br />- [Sprial model (video)](https://www.youtube.com/watch?v=mp22SDTnsQQ) <br />- [Scrum model (video)](https://www.youtube.com/watch?v=rGhRMzz0WVU)| - [Activity 03 - Software Lifecycle Activity](https://drive.google.com/drive/folders/11NP0egJMVHQDX0_rgpcuGGnLprXxHquR?usp=sharing) | - [Homework 2 - FOSS Fieldtrip](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Homeworks/Homework_02_-_FOSS_Field_Trip.odt) Individual submission to Kodiak
||**Week 3 - Open Source**|
9/14 <br />**H 103** | | - Review Part 2 from Team Assn 2 <br> - [Activity 04 - Communication in Projects](https://drive.google.com/drive/folders/1NbMyO2uNmu6eokZ24PtAJwWCdxYLR7G-?usp=sharing) <br /> - **Start Team Assn 3** | - [Team Assn 2 - BNM Exploration](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Project/Assn_2_-_BNM_GitLab_Exploration.odt) Team submission to Kodiak
9/16 <br />**Discord** | - [Open Source Software Basics (there are cookies!)](https://www.youtube.com/watch?v=Tyd0FO0tko8&feature=youtu.be) <br/> - [Introduction to Free and Open Source Software](http://teachingopensource.org/practical-oss-exploration/introduction-free-open-source-software/) <br/> - [What is Free Software](http://www.gnu.org/philosophy/free-sw.html) from the Free Software Foundation <br /> - [The Open Source Definition](http://opensource.org/osd) from the Open Source Initiative  | - [Activity 05 - FOSS Community Principles](https://drive.google.com/drive/folders/1P3xaKbgFXkdVUME4_xp-bPEuHmnlOb01?usp=sharing) 
||**Week 4 - Open Source & Requirements**|
9/21 <br />**H103**| - [Chapter 3 The Lay of the Land](http://teachingopensource.org/practical-oss-exploration/the-lay-of-the-land/) from Practical OSS Exploration <br/> - [Interacting with a FOSS community](https://www.snoyman.com/blog/2017/10/effective-ways-help-from-maintainers "Interacting with a FOSS Community") | - [Activity 06 - FOSS Community Scenarios](https://drive.google.com/drive/folders/1uo5y44tnPNDS0ql8yTA9IeUl6eJTbniO?usp=sharing) <br /> - **Start Team Assn 4**  | - [Team Assn 3 - BNM Project StartUP - Part 1](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Project/Assn_3_-_BNM_Project_StartUp.odt)
9/23 <br />**Discord**| - [Writing a Software Requirements Document](https://home.adelphi.edu/%7Esiegfried/cs480/ReqsDoc.pdf) by Tanya Berezin<br /> - [Cafeteria Ordering SRS](http://csis.pace.edu/%7Emarchese/SE616_New/Samples/SE616_SRS.doc) by Karl Weigers | - [Activity 07 - Requirements Exploration](https://drive.google.com/drive/folders/1FGsExX3uYNl3UJjQK3etBNTaNNX7w2EW?usp=sharing) | - [Homework 3 - Issue Evaluation](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Homeworks/Homework_03_-_Issue_Evaluation.odt)
||**Week 5 - Requirements**|
9/28 <br />**H103**| | - [Activity 08 - SRS Activity](https://drive.google.com/drive/folders/1mnPLaV1o2MrqIJ6ZwAAm-4AT2M8kIjCV) | - [Team Assn 3 - BNM Project StartUP - Part 2](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Project/Assn_3_-_BNM_Project_StartUp.odt)
9/30 <br />**H103 or Discord**| | - Team Meetings - on your own <br /> **Work on Team Assn 4** <br />Submit Team Meeting notes by Friday 10/1 <br /> - List all questions on BNM Requirements in the [CS 490 Wiki page](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/wikis/Requirements-Questions)|
||**Week 6 - Requirements**|
10/5 <br />**H103**| | - Yvonne Bogle in class to talk about BNM  <br /> - **Each team come ready to demo your project!!**| - [Homework 04 - Maturity Evaluation](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Homeworks/Homework_04_-_Maturity_Evaluation.odt) [Spreadsheet for the Evaluation](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Homeworks/IEEE_Open_Source_Maturity_Model_-_Out.ods)
10/7 <br />**Discord**| - [Software Reviews](http://en.wikipedia.org/wiki/Software_review) from Wikipedia <br /> - [Improving Quality Through Software Inspections](http://www.processimpact.com/articles/inspects.html) by Karl E. Wiegers <br /> - [Seven Deadly Sins of Software Reviews](http://www.processimpact.com/articles/revu_sins.html) by Karl E. Wiegers | - [Activity 09 - Requirements Review Activity](https://drive.google.com/drive/folders/10IoJPmzmKsb1Lw4w9NZy8RvYRVjkPKFp?usp=sharing) | 
||**Week 7 - Reviews**|
10/12 |**FALL BREAK - No Class** | **Sign up for the AllThingsOpen Conference October 17-19** | - [Team Assn 4 - Software Requirements Specification](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Project/Assn_4_-_Software_Requirements_Specification.doc) <br /> - [IEEE 830 SRS Standard](http://www.math.uaa.alaska.edu/%7Eafkjm/cs401/IEEE830.pdf) is the basis for the SRS above. Refer to this as needed. <br /> - The Team One send to Platinum Bullets <br /> - Platinum Bullets send to Captains <br /> - Captians send to Shoguns <br /> - Shoguns send to The Team One 
10/14 <br />**H103** | - Apply the [Review Checklist](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Project/Assn_4_-_Requirements_Review_Checklist.doc) to the SRS you received - individual | - In-class Reviews <br /> - Bring individual completed checklist applied to the SRS you received |
||**Week 8 - Software Architectures**|
10/19 <br />**H103**| | **Team Meetings** <br /> 9:30 - The Team One <br /> 9:50 - Platnium Bullets <br /> 10:10 - Captains <br /> 10:30 - Shoguns |
10/21 <br />**Discord** | - [List of Software Architectures](https://en.wikipedia.org/wiki/List_of_software_architecture_styles_and_patterns) Wikipedia <br /> - [Architectural Design](https://www.geeksforgeeks.org/software-engineering-architectural-design/) Geeks for Geeks <br /> - [Architectural Styles](https://www.thomasalspaugh.org/pub/fnd/architecture.html) Thomas Alspaugh  - Software Architecture <br /> - [What is a Microservices Architecture](https://searchapparchitecture.techtarget.com/definition/microservices) <br /> - [Microservices](https://www.youtube.com/watch?v=SouNISAnXlo&ab_channel=MuleSoftVideos)||  - [Team Assn 5 - Review Report](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Project/Assn_5_-_Review_Report.doc) submit to Kodiak <br /> - [Team Assn 4 - Software Requirements Specification](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Project/Assn_4_-_Software_Requirements_Specification.odt) submit revised version to Kodaik
||**Week 9 - Design**|
10/26 <br />**H103**| - [Six Steps to Understanding a Codebase...](https://blog.end-game.com/six-steps-to-understanding-a-code-base-super-quick-bb7294b41) <br /> - [How to understand a large codebase](https://sparkbox.com/foundry/how_to_understand_a_large_codebase) <br /> - [Stackexchange hints on how to understand a large codebase](https://softwareengineering.stackexchange.com/questions/6395/how-do-you-dive-into-large-code-bases)  | - [Activity 10 - Software Architecture Activity](https://drive.google.com/drive/folders/1eIlCn96h9FlQvugDA36w5IoE117FY72a?usp=sharing) | - [Homework 5 - Conference](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Homeworks/Homework_05_-_Conference.odt)
10/28 <br />**Discord**| - [Seven Principles of Software Development](https://lingualeo.com/en/jungle/seven-principles-of-software-development-by-david-hooker-48432) David Hooker <br /> - [Meeting Scheduler System](https://gitlab.com/heidijcellis/cs-490/-/blob/master/Readings/Meeting_Scheduler_System.doc) Read before class <br /> - [Evaluation Criteria for Meeting Scheduler System](https://gitlab.com/heidijcellis/cs-490/-/blob/master/Readings/Evaluation_Criteria.doc) read before class| - [Activity 11 - Design Activity](https://drive.google.com/drive/folders/1qGKFzUJPJY94Xa_tAAhetrr_HxmpKOYL?usp=sharing) | 
||**Week 10** - Testing|
11/2 <br />**Discord**|-  [Killed by a Machine - the THERAC-25](https://hackaday.com/2015/10/26/killed-by-a-machine-the-therac-25/) <br /> - [Introduction to Software Testing - UMinn](https://www.coursera.org/lecture/software-processes/software-testing-introduction-XmIRH)<br /> - [Why is Testing Hard? - UMinn](https://www.coursera.org/lecture/software-processes/software-testing-introduction-XmIRH) | - [Activity 12 - Intro to Testing Activity](https://drive.google.com/drive/folders/1Xac2Dc1STwHmKH2Om6A6mk1HbEHnFtJQ?usp=sharing) |
11/4 <br />**H103** | - [System Testing](http://www.guru99.com/system-testing.html) from Guru99 <br /> - [A Beginners Guide to System Test](http://www.softwaretestinghelp.com/system-testing/) by Software Testing Help</td> | - [Activity 13 - White Box Testing Activity](https://drive.google.com/drive/folders/1Mwe2LXXpc1niwky8Y-hz5arX8GyA8vVF?usp=sharing) | - [Homework 6 - Team Retrospective](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Homeworks/Homework_06_-_Team_Retrospective.odt)
||**Week 11**|
11/9 <br />**H103** | - [What is CI/CD](https://www.redhat.com/en/topics/devops/what-is-ci-cd) from Red Hat | - [Activity 14 - Testing Activity](https://drive.google.com/drive/folders/1Reo4PZDHLbDGGjmzS05sdZmYQDICjfgT?usp=sharing)| 
11/11 | **Dr. Ellis away. You may work in H103 or on Discord.** Try to get activities 15-17 completed | - [Activity 15 - Getting Started with Docker](https://drive.google.com/drive/folders/1Z3ly0PaN8AB-9zozGiNxHgMxrlqG393X?usp=sharing) <br /> - [Activity 16 - Docker Images and Containers](https://drive.google.com/drive/folders/1HKVHrOftZ0L07dVxWpgqa0-AJOg5x0gL?usp=sharing) <br /> - [Activity 17 - Docker Port Publishing](https://drive.google.com/drive/folders/1bbPQutTAPUzTd74Dm4xvNSPkEJBvAW1k?usp=sharing) | - [Team Assn 6 - Design](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Project/Assn_6_-_Design.doc)
||**Week 12 - Docker and Microservices**|
11/16 <br />**H103** | - [Microservices Architecture](https://docs.microsoft.com/en-us/dotnet/architecture/microservices/architect-microservice-container-applications/microservices-architecture) from Microsoft <br /> - [What is Microservices architecture](https://cloud.google.com/learn/what-is-microservices-architecture) from Google| - [Activity 18 - Docker Bind Mounting](https://drive.google.com/drive/folders/1pmKidMLHuyyyNABf5ieiBA4r-oZaJUWN?usp=sharing) <br /> - [Activity 19 - Getting Started with Docker Compose](https://drive.google.com/drive/folders/1dcUtC7w87REPdiMfm9kc9n03M_8E0jzA?usp=sharing) 
11/18 <br />**Discord** | |  - [Activity 20 - Microservices Architecture](https://drive.google.com/drive/folders/1ZgX9qfDRgpm82uUfe-ANIhnosDzZI_Fh?usp=sharing) <br /> - [Activity 21 - Intro to REST APIs](https://drive.google.com/drive/folders/1TOMunGWiLVmfFIJYOLstjj2iJOsSaEcP?usp=sharing)| 
||**Week 13 REST**|
11/23 <br />**Discord**  | | - Finish [Activity 21 - Intro to REST APIs](https://drive.google.com/drive/folders/1TOMunGWiLVmfFIJYOLstjj2iJOsSaEcP?usp=sharing) | - [Homework 7 - Container Practice](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Homeworks/Homework_07_-_Container_Practice.odt) <br /> - Watch [this video](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Homeworks/zoom_0.mp4) to see Dr. Jackson walk through the steps of the homework 
11/25 | **THANKSGIVING - No Class** | |
||**Week 14**|
11/30 <br />**H103** | - [What is REST](https://restfulapi.net/) <br /> - [REST APIs](https://www.ibm.com/cloud/learn/rest-apis) <br /> - [Intro to Swagger](https://www.javatpoint.com/swagger) | |
12/2 <br />**Discord** | |  - [Activity 22 - REST API Calls](https://drive.google.com/drive/folders/1wbLzYXsYCWL2CuhQlzEB-GmMzFLhCsMo?usp=sharing)|
||**Week 15**|
12/7 <br />**Discord** | | - [Activity 23 - Implementing New REST API Calls](https://drive.google.com/drive/folders/1XAuYhRBOF3jkxUjlmLMqJpHtQfdQ2HN2?usp=sharing) <br /> - Course evaluations|
12/9 <br />**103** | | Final Presentations | - [Final Presentation Assignment](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Project/Assn_7_-_Presentation.odt)
|**Finals Week** | | | - [Homework 8 - REST API Design](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Homeworks/Homework_08_-_REST_API.odt) - Due 9:00 AM Monday December 13th via Kodiak. <br /> - [Student Evaluation of Team and Project](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Project/Student_Evaluation_of_Project_and_Team.doc) - Due 9:00 AM Monday December 13th - email to Dr. Ellis
<!-- 


<!================= WEEK 12 =========================!>
<td colspan="5" bgcolor="#339999">
<center>Week 12: More Testing and Estimation</center>
</td>
</tr>
<tr bgcolor="#339999">
<th valign="top" align="center">Date</th>
<th valign="top" align="center">Reading Before Class</th>
<th width="22%" valign="top" align="center">Activity in Class</th>
<th width="22%" valign="top" align="center">DUE Today</th>
<th width="23%" valign="top" align="center">ASSIGNED Today</th>
</tr>
<tr>
<td>Tues, 11/17 (Zoom) </td>
<td>More Testing</td>
<td>- [Coffee Maker Testing Activity](https://drive.google.com/drive/folders/1xSGccTGEcSeCNjvzWpSj84cnoMzYB0XT)</td>
<td></td>
<td> </td>
</tr>
<tr>
<td>Thurs, 11/19 (Zoom)</td>
<td>
- [Toptal's Introduction to Software Estimation](https://www.toptal.com/agile/software-costs-estimation-in-agile-project-management) Read only to the section titled "Our approach to software costs and pricing"
<br />
- [Overview of CoCoMo Model](http://www.softstarsystems.com/overview.htm) [-](http://www.guru99.com/system-testing.html)
</td>
<td>
- [Cost Estimation Activity](https://drive.google.com/drive/folders/1E6Llyz14llBy1gkgerp2At5gwTVSl_JX)
</td>
<td></td>
<td>
</td>
</tr>
<tr>
<!================= WEEK 13 =========================!>
<td colspan="5" bgcolor="#339999">
<center>Week 13: Ethics - Zoom</center>
</td>
</tr>
<tr bgcolor="#339999">
<th valign="top" align="center">Date</th>
<th valign="top" align="center">Reading Before Class</th>
<th width="22%" valign="top" align="center">Activity in Class</th>
<th style="height: 19px;" width="22%" valign="top" align="center">DUE Today</th>
<th width="23%" valign="top" align="center">ASSIGNED Today</th>
</tr>
<tr>
<td>Tues, 11/24 (Zoom)</td>
<td>- [ACM Code of Ethics and Professional Practice](http://www.acm.org/about-acm/acm-code-of-ethics-and-professional-conduct)</td>
<td>
<b>Quiz 4</b>
<br />
- [Ethics Activity](https://drive.google.com/drive/folders/1x-32Z81dOb1NGgeM5vaNE_PTlqH8VOmD)
</td>
<td> </td>
<td> 
- [Student Evaluation of Project and Team](https://gitlab.com/heidijcellis/cs-490/-/blob/master/Team%20Assignments/Student_Evaluation_of_Project_and_Team.doc) <b>Due:</b> 5:00 PM Mon 12/7 <b>Submit:</b> One per person, email to Dr. Ellis
<br />
- [Project Presentation](https://gitlab.com/heidijcellis/cs-490/-/blob/master/Team%20Assignments/Presentation_Guidelines.doc) <b>Due:</b> 9:30 AM Thursday 12/3 <b>Submit:</b> Copy of slides, one per team. 
<br />
- [Team Assn 7 - GitLab Reorg](https://gitlab.com/heidijcellis/cs-490/-/blob/master/Team%20Assignments/Assn_7_-_GitLab_Reorg.odt) <b>Due:</b> 9:30 AM Wednesday 12/9 <b>Submit:</b> Team Word or ODT document via Kodiak 
</td>
</tr>
<tr>
<td>Thurs, 11/26</td>
<td><font color="red">Thanksgiving - No Class!!</font></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<!================= WEEK 14 =========================!>
<td colspan="5" bgcolor="#339999">
<center>Week 14: Presentations and Wrap Up - Zoom</center>
</td>
</tr>
<tr bgcolor="#339999">
<th width="8%" valign="top" align="center">Date</th>
<th width="25%" valign="top" align="center">Reading Before Class</th>
<th width="22%" valign="top" align="center">Activity in Class</th>
<th width="22%" valign="top" align="center">DUE Today</th>
<th width="23%" valign="top" align="center">ASSIGNED Today</th>
</tr>
<tr>
<td>Tues, 12/1</td>
<td>- [The Business Case for Better Software Practices](https://docplayer.net/17964994-Business-case-for-better-software-practices.html)</td>
<td>Why Software Engineering? Wrap Up</td>
<td></td>
<td> </td>
</tr>
<tr>
<td>Thurs, 12/3</td>
<td style="height: 57px;"> </td>
<td>
Final Project Presentations 
</td>
<td></td>
<td> </td>
</tr>
<!================= WEEK 15 =========================!>
<tr>
<td colspan="5" bgcolor="#339999">
<center>Week 15: </center>
</td>
</tr>
<tr bgcolor="#339999">
<th width="8%" valign="top" align="center">Date</th>
<th width="25%" valign="top" align="center">Reading Before Class</th>
<th width="22%" valign="top" align="center">Activity in Class</th>
<th width="22%" valign="top" align="center">DUE Today</th>
<th width="23%" valign="top" align="center">ASSIGNED Today</th>
<tr>
<td>Mon 12/7</td>
<td> </td>
<td> </td>
<td> Student Evaluation of Project and Team</td>
<td> </td>
-->
