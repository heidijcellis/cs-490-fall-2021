# [Current version of the course](https://gitlab.com/heidijcellis/cs-490-fa-2024)

# Welcome to CS 490 Fall 2021

Let me start by setting your expectations for this course. **This course will be different from most (all?) of your other courses!!**  This means that you should reframe your thinking about CS 490 from a "couse" to a "job". I view CS 490 as your first steps into professional software development and I have professional expectations including:
- Things will change throughout the semester including the project content, assignments, and more. 
- You will attend all meetings/events early or on time. 
- You will communicate with your team mates and myself in a timely fashion. 
- I will not have all of the answers to your questions
- You will do significant learning outside of class. In other words, to find answers on your own. 
    - Yes, I will help you find answers, but you will have to do the learning independently. 
    - **Note:** You may find this frustrating. I would rather have you learn this skill now and be prepared for real-world software development than for you to learn via mistakes in your first position. 

## Before Class Starts:
We'll be working mostly in GitLab, using Kodiak only for some submissions. To get started: 

1. Read the [Syllabus.MD](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Syllabus.MD) file
2. Read the [Schedule.MD](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Schedule.MD) file
3. Read and follow the directions in the [Discord Guidelines - CS 490](https://gitlab.com/heidijcellis/cs-490-fall-2021/-/blob/main/Discord_Guidelines_-_CS_490.pdf) file

Some [Important Links](https://gitlab.com/heidijcellis/cs-490/-/blob/master/ImportantLinks.md)

Looking forward to seeing you!
Heidi Ellis 


